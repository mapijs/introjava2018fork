package methodsDemo;



public class ShapeAnalysis {

    public static double getSurfaceAreaOfCircle(double radius) {
        double surface = Math.PI * Math.pow(radius, 2);
        return surface;
    }
}
