package runescape;

public class CombatTypeAccuracyBoostCalculator {
    /**
     * raises or lowers player's accuracy depending on used combatType and computer's combatType.
     */

    void calculateCombatTypeAccuracyBoost() {

    if (playerCombatType.equals(computerCombatType) {
            accuracyBoost = 1;
        }
    else if (playerCombatType.equals("ranged") && computerCombatType.equals("magic")){
        accuracyBoost = 1.25;
    }

    else if (playerCombatType.equals("ranged") && computerCombatType.equals("melee")){
        accuracyBoost = 0.75;
    }

    else if (playerCombatType.equals("magic") && computerCombatType.equals("melee")){
        accuracyBoost = 1.25;
    }

    else if (playerCombatType.equals("magic") && computerCombatType.equals("ranged")){
        accuracyBoost = 0.75;
    }

    else if (playerCombatType.equals("melee") && computerCombatType.equals("ranged")){
        accuracyBoost = 1.25;
    }

    else if (playerCombatType.equals("melee") && computerCombatType.equals("magic")){
        accuracyBoost = 0.75;
    }


}
}



