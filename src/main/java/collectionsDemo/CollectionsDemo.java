package collectionsDemo;

import nl.bioinf.nomi.hello.Horse;

import java.util.ArrayList;
import java.util.List;

public class CollectionsDemo {
    public static void main(String[] args) {
        List<Horse> horses = createHorseList();
        System.out.println("horse2 = " + horses.get(1));
        horses.remove(1);
    }
    public static List createHorseList() {
        Horse horse1 = new Horse();
        horse1.furColor = "brown";
        horse1.weightInKilograms = 644;

        Horse horse2 = new Horse();
        horse1.furColor = "eight";
        horse1.weightInKilograms = 5;


        ArrayList<Horse> horses = new ArrayList<>();
        horses.add(horse1);
        horses.add(horse2);
        //List<Horse> horses = List.of(horse1, horse2);
        //list.of unmodifiable
        return horses;
    }

}
