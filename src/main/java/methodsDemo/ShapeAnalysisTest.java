package methodsDemo;

import static org.junit.jupiter.api.Assertions.*;

class ShapeAnalysisTest {

    @org.junit.jupiter.api.Test
    void getSurfaceAreaOfCircle() {
        System.out.println("start test");
        double input = 3;
        double expected = Math.PI * (input *input);
        double observed = ShapeAnalysis.getSurfaceAreaOfCircle(input);
        assertEquals(expected, observed);


         input = 0;
         expected = Math.PI * (input *input);
         observed = ShapeAnalysis.getSurfaceAreaOfCircle(input);
        assertEquals(expected, observed);
        System.out.println("end test");
    }
}