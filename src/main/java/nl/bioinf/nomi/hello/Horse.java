package nl.bioinf.nomi.hello;

/**
 * Models a horse, you know, found in sausages.
 * @author me
 */

public class Horse {
    public int weightInKilograms;
    public String furColor;

    /**
     * makes the horse gallop at the given speed.
     * @param speedInKmPerHour the speeeeeeeed.
     */
    void gallop(double speedInKmPerHour) {
        System.out.println("Horse galloping at " + speedInKmPerHour + " km per hour");
    }
    public String toString(){
        return " Horse: {color :" + furColor + ", weight =" + weightInKilograms + "}";
    }
}
