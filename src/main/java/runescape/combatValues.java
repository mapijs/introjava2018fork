package runescape;

import java.util.Dictionary;
import java.util.Hashtable;



public class combatValues {
    /*
    Create dictionaries for the three styles of combat.
    */
    Dictionary magicDict = new Hashtable();
    Dictionary meleeDict = new Hashtable();
    Dictionary rangedDict = new Hashtable();

    /*
    Add weapons with damage values to the corresponding dictionaries.
    */
    private void magicValues() {
        magicDict.put("Air staff", 150);
        magicDict.put("Water staff", 300);
        magicDict.put("Earth staff", 450);
        magicDict.put("Noxious staff", 600);
    }

    private void meleeValues() {
        meleeDict.put("Rune longsword", 150);
        meleeDict.put("Dragon scimitar", 300);
        meleeDict.put("Dharok's greatAxe", 450);
        meleeDict.put("Armadyl godsword", 600);
    }

    private void rangedValues() {
        rangedDict.put("Rune crossbow", 150);
        rangedDict.put("Royal crossbow", 300);
        rangedDict.put("Ascension crossbow", 450);
        rangedDict.put("Noxious longbow", 600);
    }
}